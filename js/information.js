
// Information

let infoBgInterval;

$("#information .info_wrap dl").on('click', function(){
    NavAction.lockScroll();

    var element = $("#information ul").eq( $(this).index() );
    var elementChildLen = element.children().length;

    if ( !elementChildLen ) {
        return;
    }

    element.fadeIn();
    element.children().eq(0).css('opacity', 1).siblings().css('opacity', 0);
    $("#information .infobg").fadeIn();

    let index = 0;

    infoBgInterval = setInterval(function(){

        index = ( ++index >= elementChildLen ) ? 0 : index;

        element.children().eq(index).css('opacity', 1).siblings().css('opacity', 0);

    }, 3000);

});
$("#information .infobg .close").on('click', function(e){
    e.preventDefault();
    NavAction.releaseScroll();

    $("#information .infobg").fadeOut();

    if ( infoBgInterval ) {
        clearInterval( infoBgInterval );
    }

    $("#information .infobg ul").hide();

});
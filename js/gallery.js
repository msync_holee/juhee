let galleryData = [
    {img: 'd11.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd16.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w06.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd28.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w03.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd09.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'd37.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd31.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd17.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w08.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd30.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'd15.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w21.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd23.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w02.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd24.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w12.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd25.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w07.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd10.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'd12.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'w17.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd35.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'd27.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w14.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd06.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w10.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'w05.jpg', filter: 'wedding', w: 1, h: 1, d: 'v'},
    {img: 'w20.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd07.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w01.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd26.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w13.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd36.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w18.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd08.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'd13.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd01.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd02.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd34.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w09.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd04.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w11.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd32.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'd20.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'd22.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd03.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w19.jpg', filter: 'wedding', w: 1, h: 2, d: 'v'},
    {img: 'd33.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w15.jpg', filter: 'wedding', w: 1, h: 1, d: 'v'},
    {img: 'd14.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'w16.jpg', filter: 'wedding', w: 1, h: 1, d: 'v'},
    {img: 'd21.jpg', filter: 'date', w: 1, h: 1, d: 'v'},
    {img: 'd29.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd18.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd19.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
    {img: 'd05.jpg', filter: 'date', w: 1, h: 2, d: 'v'},
];


const Gallery = (() => {
    return {
        iso: null,
        init( data ) {

            this.iso = new Isotope(
                document.querySelector("#gallery .grid"),
                {
                    itemSelector: '.grid-item',
                    percentPosition: true,
                    layoutMode: 'masonry',
                    masonry: {
                        columnWidth: '.grid-sizer'
                    }
                }
            );
            
            let galleryHTML = '';

            for ( let dt of data ) {
                var img = `./gallery/${dt.filter}/${dt.img}`;
                galleryHTML += `<li class="swiper-slide"><img src="${img}"></li>`;
            }
            $("#gallery .viewer .lists").html(galleryHTML);

            return this;

        },
        insertElement(data) {
            let div = document.createElement('div');
            div.classList.add('grid-item');

            if ( data.filter ) {
                div.classList.add(data.filter);
            }

            // if ( data.w == 2 ) {
            //     div.classList.add('grid-item--width2');
            // }
            // if ( data.h == 2 ) {
            //     div.classList.add('grid-item--height2');
            // }

            div.classList.add('grid-item--height2');

            var img = `./gallery/${data.filter}/${data.img}`;
            div.innerHTML = `<div style="background-image: url(${img});"></div>`;

            document.querySelector('#gallery .grid').appendChild(div);

            this.iso.appended( div );
            this.iso.layout();
        },
        appendData(data) {
            let _this = this;
            if ( data ) {
                data.forEach((item) => {
                    _this.insertElement(item);
                });
            }
        },
        viewerIdx: 1,
        gallerySwiper: null,
        start(idx = 1) {
            
            this.viewerIdx = idx;
            NavAction.lockScroll();

            let img = document.querySelector(`#gallery .viewer .lists li:nth-of-type(${this.viewerIdx}) img`).getAttribute('src');
         
            document.querySelector("#gallery .viewer .bg").style.backgroundImage = `url(${img})`;
            // document.querySelector("#gallery .viewer .lists").style.transform = `translateX(-${((this.viewerIdx - 1) * 100)}vw)`;

            document.querySelector("#gallery .viewer").style.display = 'block';

            this.gallerySwiper = new Swiper(".viewer", {
                initialSlide: idx - 1,
                navigation: {
                    nextEl: '.next',
                    prevEl: '.prev',
                },
                loop: true
            });

            this.gallerySwiper.on('slideChange', function(e){
                let img = document.querySelector(`#gallery .viewer .lists li:nth-of-type(${this.activeIndex + 1}) img`).getAttribute('src');
                let imgElem = new Image();
                imgElem.onload = () => {
                    document.querySelector("#gallery .viewer .bg").style.backgroundImage = `url(${img})`;
                };

                imgElem.src = img;
            })
        },
        break() {
            document.querySelector("#gallery .viewer").style.display = 'none';
            this.gallerySwiper.destroy(true, true);
            NavAction.releaseScroll();
        },
        prev() {
            let currentIndex = this.viewerIdx - 1;

            if ( currentIndex > 0 ) {
                this.start(currentIndex);
            }

        },
        next() {
            let currentIndex = this.viewerIdx + 1;
            let maxImages = document.querySelectorAll("#gallery .viewer .lists li").length;

            if ( currentIndex < maxImages ) {
                this.start(currentIndex);
            }

        }
    }
})().init(galleryData);

$("#gallery .filter a").on('click', function(e){
    e.preventDefault();
    let filter = this.getAttribute("data-filter");
    Gallery.iso.arrange({filter: filter});
    $(this).addClass('on').siblings().removeClass('on');
});

let page = 0;
$("#gallery > a").on('click', function(e){
    e.preventDefault();

    let start = page * 10;
    let end = start + 10;

    if ( $(this).hasClass('fold') ) {
        $(this).text('더 많은 사진보기').removeClass('fold');
        page = 0;
        $(".grid-item").remove();
        start = 0;
        end = 10;
        NavAction.moveTo('#gallery');
    } else if ( end >= galleryData.length ) {
        end = galleryData.length;
        $(this).text('접어두기').addClass('fold');
    }
    
    Gallery.appendData( galleryData.slice(start, end) );
    
    page += 1;
}).trigger('click');

$("#gallery").on('click', '.grid-item', function(e){
    e.preventDefault();
    Gallery.start( $(this).index() );
});

$("#gallery .close").on('click', function(e){
    e.preventDefault();
    Gallery.break();
});
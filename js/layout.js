// Navigation

let scrollInterval;

const NavAction = (() => {
    return {
        init() {
            this.sectionTopPosition();

            this.audioElem = document.querySelector("#backgroundMusic");
            this.audioElem.volume = 0.3;
            this.audioElem.loop = true;

            this.toggleMusic();

            // document.querySelector("#audioToggler").style.backgroundImage = 'url(./images/btn_sound_off.png)';

            return this;
        },
        moveTo( menu = '#main' ) {
            $("html, body").animate({scrollTop: $(menu).offset().top}, 500);
        },
        sectionTopPosition() {
            
            let found = 0;

            for ( let elem of [...document.querySelectorAll('section')].reverse() ) {
                let id = elem.id;
                let navElem = document.querySelector(`a[href='#${id}']`);
                
                if ( navElem === null ) continue;

                if ( elem.getBoundingClientRect().y <= 0 && found === 0 ) {
                    navElem.classList.add('on');
                    found = 1;
                } else {
                    navElem.classList.remove('on');
                }
            }
        },
        lockScroll() {
            window.lastScroll = $(window).scrollTop();
            document.body.classList.add('stop');
        },
        releaseScroll() {
            document.body.classList.remove('stop');
            $("html, body").scrollTop( window.lastScroll );
        },
        audioElem: null,
        toggleMusic() {
            if ( this.audioElem.paused ) {
                this.audioElem.play();
                document.querySelector("#audioToggler").style.backgroundImage = 'url(./images/btn_sound_on.png)';
            } else {
                this.audioElem.pause();
                document.querySelector("#audioToggler").style.backgroundImage = 'url(./images/btn_sound_off.png)';
            }
        },
        leafAnimation( elem ) {
            if ( elem ) {
                elem.style.opacity = Math.random();

                elem.style.left = window.outerWidth * Math.random() + 'px';
                elem.style.top = window.outerHeight * Math.random() + 'px';

                elem.style.transform = `rotate(${360 * Math.random()}deg)`;
                

                setTimeout(() => {this.leafAnimation(elem)}, 5000 + (15000 * Math.random()));
            } else {
                for ( let leafElem of document.querySelectorAll('.leaf img') ) {
                    this.leafAnimation( leafElem );
                }
            }
        },
    }
})().init();


// Set navigation
document.querySelector("nav").addEventListener('click', function(){
    if ( window.innerWidth < 768 ) {
        if ( this.classList.value === 'active' ) {
            this.classList.remove('active');
            NavAction.releaseScroll();
        } else {
            this.classList.add('active');
            NavAction.lockScroll();
        }
    }
});
$("nav a").on('click', function(e){
    e.preventDefault();
    NavAction.moveTo( $(this).attr('href') );
});

document.querySelector('.btn_top').addEventListener('click', function(e){
    e.preventDefault();

    NavAction.moveTo( '#main' );
});

const copyURL = new ClipboardJS('#copyurl', {
    text: function(trigger){
        return window.location.origin;
    }
});

copyURL.on('success', function(e){

    $(".pop.show").removeClass('show');
    
    document.querySelector('.copyurl').classList.add('show');

    setTimeout(function(){
        document.querySelector('.copyurl').classList.remove('show');
    }, 2000);
});

$("#audioToggler").on('click', function(e){
    e.preventDefault();
    NavAction.toggleMusic();
});

$("#call").on('click', function(e){
    e.preventDefault();
    $(".pop.show").removeClass('show');
    $(".pop.call").addClass('show');
});

$("#mail").on('click', function(e){
    e.preventDefault();
    $(".pop.show").removeClass('show');

    $(".pop.mail").addClass('show');
});

$("body").on('click', function(e){
    if ( $(e.target).closest('.quick').length == 0 ) {
        $(".pop.show").removeClass('show');
    }
})

let scrollDirection = 0;
let beforeScrollY = window.scrollY;

const headerElem = document.querySelector("header");

let mobileMode = false;

window.onresize = (e) => {
    if ( window.innerWidth <= 768 ) {
        mobileMode = true;
    } else {
        mobileMode = false;
    }
}


const PopupClose = (day) => {
    if ( day ) {
        setCookie('popup', '1', 1);
    }

    $(".popup").hide();
}

$(function(){

    $("#message .lists").on('mouseover', function(e){
        $(this).css('animation-play-state', 'paused');
    }).on('mouseleave', function(e){
        $(this).css('animation-play-state', 'running');
    });

    // 팝업작동
    // if ( !getCookie('popup') && window.innerWidth > 768 ) {
    //     $(".popup").css('display', 'flex');
    // }
});

let windowBottom = 0;

// 슬라이드 업 애니메이션을 동작해야 하는 요소 갯수
const waitSlideUpElem = $(".animate-slide-up").length;
// 슬라이드 업 애니메이션이 완료 된 요소 갯수
let startSlideUpElem = 0;

$(window).scroll(function(){

    let windowBottom = $(this).scrollTop() + $(this).height();

    NavAction.sectionTopPosition();

    if ( beforeScrollY > window.scrollY ) {
        scrollDirection = 1;
        headerElem.classList.add('show');
    } else {
        scrollDirection = -1;
        headerElem.classList.remove('show');
    }

    beforeScrollY = window.scrollY;

    // 스크롤하면 열려있는 메뉴가 닫힘
    document.querySelector('nav').classList.remove('active');


    // btn top
    $(".btn_top").css('display', $(this).scrollTop() > 10 ? 'block' : 'none' );

    let topLimit = $("#thanks").offset().top;
    if ( windowBottom > topLimit ) {
        $(".btn_top").css({
            position: "absolute",
            bottom: "auto",
            top: topLimit - $(".btn_top").height() - 60,
        });
    } else {
        $(".btn_top").css({
            position: "fixed",
            bottom: "65px",
            top: "auto"
        });
    }

    // 화면에 보이면 위로 올라오는 애니메이션
    if ( startSlideUpElem < waitSlideUpElem ) {
        $(".animate-slide-up").filter(function(){
            // 이미 보이는 요소는 다시 계산하지 않음
            return !$(this).hasClass('start');
        }).each(function(i){
            if ( $(this).offset().top < windowBottom ) {
                $(this).addClass('start');
                startSlideUpElem += 1;
            }
        });
    }
}).trigger('scroll');




//<!-- Main script START

// 메인 이미지 로드를 기다린다.
$("#main .photo div").imagesLoaded({background: true})
// 로딩이 완료되면 실행
.done(function(){

    $("#main .photo").addClass('start');
    
    // #main text
    setTimeout(function(){
        doAnimation("#main .tit_line", 1);
        doAnimation("#main .tit01, #main .tit02, #main .tit03", 2);
        doAnimation("#main .tit04 > div:nth-of-type(1)", 3);
        doAnimation("#main .tit04 > div:nth-of-type(2)", 3.3);
        doAnimation("#main .tit04 > div:nth-of-type(3)", 3.6);

        NavAction.releaseScroll();
        $(".quick").addClass('start');
        $("#main .bottom").addClass('view');
    }, 1000);

    let mainIndex = 0;
    const mainMax = $("#main .photo > div").length;
    setInterval(function(){
        mainIndex = ( ++mainIndex >= mainMax ) ? 0 : mainIndex;
        $("#main .photo > div").eq( mainIndex ).addClass('show').siblings().removeClass('show');
    }, 4000);
});

const doAnimation = ( selector, sec ) => {
    setTimeout(function(){
        $(selector).addClass('start');
    }, sec * 1000 );
}

// Main script END -->
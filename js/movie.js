const theater = document.querySelector("#movie .theater");
let is_music_play = false;

for ( let elemMovie of document.querySelectorAll("#movie li") ) {
    elemMovie.addEventListener('click', function(e){

        // if ( window.innerWidth <= 768 ) return;
        
        NavAction.lockScroll();

        theater.style.display = 'flex';


        theater.innerHTML = `
            <a href="#" class="close"></a>
            <video src="${this.childNodes[3].src}" controls autoplay></video>
        `;

        document.querySelector("#movie .theater video").play();
        if ( !NavAction.audioElem.paused ) {
            NavAction.audioElem.pause();
            is_music_play = true;
        }
        document.querySelector("#movie .theater a").addEventListener('click', function(e){
            e.preventDefault();
            theater.style.display = 'none';
            theater.innerHTML = '';

            if ( is_music_play === true ) {
                NavAction.audioElem.play();
                is_music_play = false;
            }
            
            NavAction.releaseScroll();

        });
    });
}
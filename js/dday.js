
const wedding_date = new Date('2020-07-04T10:50:00');
// const love_start = new Date('2018-10-22T00:00:00');
const love_start = new Date('2018-03-12T00:00:00');

let CountDayInterval = setInterval(function(){
    let now = new Date();
    let loveDate = Math.ceil( (now - love_start) / ( 24 * 60 * 60 * 1000 ) );
    let leftSec = Math.floor( (wedding_date - now) / 1000 );
    let leftDate = Math.floor( leftSec / ( 24 * 60 * 60 ) );

    leftSec -= leftDate * ( 24 * 60 * 60 );

    let leftHour = Math.floor( leftSec / ( 60 * 60 ) );

    leftSec -= leftHour * ( 60 * 60 );

    let leftMin = Math.floor( leftSec / 60 );

    leftSec -= leftMin * 60;

    leftDate = NumberTo2Digit(leftDate);
    leftHour = NumberTo2Digit(leftHour);
    leftMin = NumberTo2Digit(leftMin);
    leftSec = NumberTo2Digit(leftSec);

    [...document.querySelectorAll("#counter .d-day span")].forEach((elem, idx) => {
        elem.textContent = leftDate[idx];
    });
    [...document.querySelectorAll("#counter .clock span")].forEach((elem, idx) => {
        if ( idx < 2 ) {
            elem.textContent = leftHour[idx % 2];
        } else if ( idx < 4 ) {
            elem.textContent = leftMin[idx % 2];
        } else {
            elem.textContent = leftSec[idx % 2];
        }
    });

    document.querySelector("#counter .countup strong").textContent = `${loveDate}일`;

}, 1000);


const NumberTo2Digit = ( num ) => {
    return ( num < 10 ) ? '0' + num : '' + num;
}
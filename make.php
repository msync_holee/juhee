<?php

echo '<pre>';

$path = __DIR__ . '/gallery';

$handle = opendir( $path );

$json = array();

while (false !== ($dir = readdir($handle))) {
    if ( strpos( $dir, '.' ) === false ) {

        $sub_path = $path . '/' . $dir;

        $sub_handle = opendir( $sub_path );
        
        while ( false !== ( $file = readdir( $sub_handle ) ) ) {
            if ( strpos( $file, '.' ) > 0 ) {

                $file_path = $sub_path . '/' . $file;

                list($w, $h) = getimagesize($file_path);

                $data = array(
                    'w' => $w,
                    'h' => $h,
                    'd' => ( $w > $h ) ? 'h' : 'v',
                    'name' => $file,
                    'path' => "./gallery/{$dir}/{$file}",
                    'filter' => $dir,
                );

                $json[] =  $data;
            }
        }
    }
}

$json_string = json_encode( $json );

$fp = fopen( $path . '/gallery.js', 'w' );
fwrite( $fp, "const galleryData = {$json_string};" );
fclose($fp);
